%bcond_with linuxdoc-tools

%define api_version 0.8.5
%define pkg_version 6.6
%global tcl_version %(echo 'puts $tcl_version' | tclsh)
%global tcl_sitearch %{_prefix}/%{_lib}/tcl%{tcl_version}
%global __provides_exclude ^(libbrltty.+\.so.*)$
%global __requires_exclude ^(libbrltty.+\.so.*)$

Summary:    Braille display driver for Linux/Unix
Name:       brltty
Version:    %{pkg_version}
Release:    9%{?dist}
License:    LGPLv2+
URL:        http://brltty.app/
Source0:    http://brltty.app/archive/%{name}-%{version}.tar.xz
Source1:    brltty.service
Source2:    brlapi-config.h
Source3:    brlapi-forbuild.h

Patch3000:  brltty-6.3-loadLibrary.patch
Patch3001:  brltty-6.3-libspeechd.patch

BuildRequires: autoconf automake glibc-kernheaders 
BuildRequires: byacc bluez-libs-devel at-spi2-core-devel alsa-lib-devel systemd gettext 
BuildRequires: espeak-ng-devel polkit-devel libicu-devel doxygen python3-docutils gpm-devel
BuildRequires: python3-setuptools
%if 0%{with linuxdoc-tools}
BuildRequires: linuxdoc-tools
%endif
Requires(post): coreutils
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Conflicts: brltty-minimal


%description
BRLTTY is a background process (daemon) which provides access
to the Linux/Unix console (when in text mode) for a blind person
using a refreshable braille display. It drives the braille display,
and provides complete screen review functionality. Some speech
capability has also been incorporated.


%package docs
Summary: Documentation for BRLTTY
License: LGPLv2+
Requires: %{name} = %{pkg_version}-%{release}
BuildArch: noarch

%description docs
This package provides the documentation for BRLTTY.

%package xw
Summary: XWindow driver for BRLTTY
License: LGPLv2+
BuildRequires: libSM-devel libICE-devel libX11-devel libXaw-devel libXext-devel libXt-devel libXtst-devel
Requires: %{name} = %{pkg_version}-%{release}
Requires: xorg-x11-fonts-misc, ucs-miscfixed-fonts

%description xw
This package provides the XWindow driver for BRLTTY.

%package at-spi2
Summary: AtSpi2 driver for BRLTTY
License: LGPLv2+
Requires: %{name} = %{pkg_version}-%{release}

%description at-spi2
This package provides the AtSpi2 driver for BRLTTY.

%package espeak-ng
Summary: eSpeak-NG driver for BRLTTY
License: LGPLv2+
Requires: %{name} = %{pkg_version}-%{release}

%description espeak-ng
This package provides the eSpeak-NG driver for BRLTTY.

%package -n brlapi
Summary: Application Programming Interface for BRLTTY
Version: %{api_version}
License: LGPLv2+
Requires(pre): glibc-common, shadow-utils
Requires(post): coreutils, util-linux
Recommends: %{name} = %{pkg_version}-%{release}

%description -n brlapi
This package provides the run-time support for the Application
Programming Interface to BRLTTY. Install this package if you
have an application which directly accesses a refreshable
braille display.

%package -n brlapi-devel
Summary: Headers, static archive, and documentation for BrlAPI
Version: %{api_version}
License: LGPLv2+
Requires: brlapi = %{api_version}-%{release}

%description -n brlapi-devel
This package provides the header files, static archive, shared object
linker reference, and reference documentation for BrlAPI (the
Application Programming Interface to BRLTTY).  It enables the
implementation of applications which take direct advantage of a
refreshable braille display in order to present information in ways
which are more appropriate for blind users and/or to provide user
interfaces which are more specifically attuned to their needs.
Install this package if you are developing or maintaining an application
which directly accesses a refreshable braille display.

%package -n tcl-brlapi
Summary: Tcl binding for BrlAPI
Version: %{api_version}
License: LGPLv2+
BuildRequires: tcl-devel
Requires: brlapi = %{api_version}-%{release}

%description -n tcl-brlapi
This package provides the Tcl binding for BrlAPI.

%package -n python3-brlapi
%{?python_provide:%python_provide python3-brlapi}
Summary: Python 3 binding for BrlAPI
Version: %{api_version}
License: LGPLv2+
BuildRequires: python3-Cython python3-devel
Requires: brlapi = %{api_version}-%{release}

%description -n python3-brlapi
This package provides the Python 3 binding for BrlAPI.

%package -n brlapi-java
Summary: Java binding for BrlAPI
Version: %{api_version}
License: LGPLv2+
BuildRequires: jpackage-utils java-devel
Requires: brlapi = %{api_version}-%{release}

%description -n brlapi-java
This package provides the Java binding for BrlAPI.

%package -n ocaml-brlapi
Summary: OCaml binding for BrlAPI
Version: %{api_version}
License: LGPLv2+
BuildRequires: make
BuildRequires: ocaml
Requires: brlapi = %{api_version}-%{release}

%description -n ocaml-brlapi
This package provides the OCaml binding for BrlAPI.

%package dracut
Summary: brltty module for Dracut
Requires: %{name} = %{pkg_version}-%{release}
Requires: dracut

%description dracut
This package provides brltty module for Dracut.

%package minimal
Summary: Stripped down brltty version for Anaconda installer
Conflicts: brltty

%description minimal
This package provides stripped down brltty version for Anaconda
installer.


%prep
%setup -qc
mv %{name}-%{pkg_version} python3
pushd python3
%autopatch -p1
rm -f Programs/brltty-ktb
popd
cp -a python3 minimal


%build
for i in -I/usr/lib/jvm/java/include{,/linux}; do
      java_inc="$java_inc $i"
done
export CPPFLAGS="$java_inc"
export CFLAGS="%{optflags} -fno-strict-aliasing $LDFLAGS"
export CXXFLAGS="%{optflags} -fno-strict-aliasing $LDFLAGS"

configure_opts=" \
  --disable-stripping \
  --without-curses \
  --without-espeak \
  --with-install-root=%{buildroot} \
  JAVA_JAR_DIR=%{_jnidir} \
  JAVA_JNI_DIR=%{_libdir}/brltty \
  JAVA_JNI=yes"

configure_opts_minimal=" \
  --disable-stripping \
  --without-curses \
  --without-speechd \
  --without-espeak \
  --disable-icu \
  --disable-polkit \
  --disable-java-bindings \
  --disable-ocaml-bindings \
  --disable-python-bindings \
  --disable-tcl-bindings \
  --disable-speech-support \
  --without-pcm-package \
  --without-midi-package \
  --with-install-root=%{buildroot} \
  --with-configuration-file=brltty-minimal.conf \
  --with-drivers-directory=%{_libdir}/brltty-minimal \
  --with-tables-directory=%{_sysconfdir}/brltty-minimal \
  --with-scripts-directory=%{_libexecdir}/brltty-minimal \
  JAVA_JNI=no"

export PYTHONCOERCECLOCALE=0


pushd minimal
./autogen
%configure $configure_opts_minimal
make
popd

pushd python3
./autogen
%configure $configure_opts PYTHON=%{__python3} CYTHON=%{_bindir}/cython
make

pushd Documents
make
popd
popd

pushd python3
  find . -name '*.sgml' |
  while read file; do
     iconv -f iso8859-1 -t utf-8 $file > $file.conv && mv -f $file.conv $file
  done
  find . -name '*.txt' |
  while read file; do
     iconv -f iso8859-1 -t utf-8 $file > $file.conv && mv -f $file.conv $file
  done
  find . -name 'README*' |
  while read file; do
     iconv -f iso8859-1 -t utf-8 $file > $file.conv && mv -f $file.conv $file
  done

  find . \( -path ./doc -o -path ./Documents \) -prune -o \
    \( -name 'README*' -o -name '*.txt' -o -name '*.html' -o \
       -name '*.sgml' -o -name '*.patch' -o \
       \( -path './Bootdisks/*' -type f -perm /ugo=x \) \) -print |
  while read file; do
     mkdir -p ../doc/${file%/*} && cp -rp $file ../doc/$file || exit 1
  done
popd



%install
mkdir -p %{buildroot}%{_libdir}/ocaml/stublibs

pushd minimal
%make_install

pushd %{buildroot}%{_libdir}/brltty-minimal
rm -f libbrlttybba.so libbrlttybxw.so libbrlttyxa2.so libbrlttysen.so libbrlttyses.so libbrlapi_java.so
popd

mv %{buildroot}%{_bindir}/brltty %{buildroot}%{_bindir}/brltty-minimal
install -d -m 755 "%{buildroot}%{_sysconfdir}"
install -p -m 644 Documents/brltty.conf "%{buildroot}%{_sysconfdir}/brltty-minimal.conf"
popd


pushd python3
%make_install JAVA_JAR_DIR=%{_jnidir} \
              JAVA_JNI_DIR=%{_libdir}/brltty \
              JAVA_JNI=yes

pushd Authorization/Polkit
%make_install
popd

install -d -m 755 "%{buildroot}%{_sysconfdir}" "%{buildroot}%{_mandir}/man5"
install -p -m 644 Documents/brltty.conf "%{buildroot}%{_sysconfdir}"
echo ".so man1/brltty.1" > %{buildroot}%{_mandir}/man5/brltty.conf.5
install -Dpm 644 %{SOURCE1} %{buildroot}%{_unitdir}/brltty.service
cp -p LICENSE* ../
rm Documents/Manual-*/*/{*.mk,Makefile*}
mv Documents/BrlAPIref/{html,BrlAPIref}
for i in Drivers/Speech/SpeechDispatcher/README \
         Documents/ChangeLog Documents/TODO \
         Documents/Manual-BRLTTY \
         Drivers/Braille/XWindow/README \
         Drivers/Braille/XWindow/README \
         Documents/Manual-BrlAPI \
         Documents/BrlAPIref/BrlAPIref \
; do
   mkdir -p ../${i%/*} && cp -rp $i ../$i || exit 1
done



rm -rf %{buildroot}/%{_libdir}/libbrlapi.a
mkdir -p %{buildroot}%{_localstatedir}/lib/brltty
touch %{buildroot}%{_sysconfdir}/brlapi.key
chmod 0640 %{buildroot}%{_sysconfdir}/brlapi.key
rm -f %{buildroot}%{_datadir}/gdm/greeter/autostart/xbrlapi.desktop
chmod 755 %{buildroot}%{_bindir}/brltty-config.sh

pushd %{buildroot}%{_includedir}/brltty
for f in config forbuild
do
  mv ./$f.h ./$f-$(getconf LONG_BIT).h
done
install -p -m 0644 %{SOURCE2} ./config.h
install -p -m 0644 %{SOURCE3} ./forbuild.h
popd

%find_lang %{name}
cp -p %{name}.lang ../

make install-dracut
popd

rm -f doc/Initramfs/Dracut/README*
rmdir doc/Initramfs/Dracut doc/Initramfs

%post
%systemd_post brltty.service

%preun
%systemd_preun brltty.service

%postun
%systemd_postun_with_restart brltty.service

%pre -n brlapi
getent group brlapi >/dev/null || groupadd -r brlapi >/dev/null

%post -n brlapi
if [ ! -e %{_sysconfdir}/brlapi.key ]; then
  mcookie > %{_sysconfdir}/brlapi.key
  chgrp brlapi %{_sysconfdir}/brlapi.key
  chmod 0640 %{_sysconfdir}/brlapi.key
fi

%files -f %{name}.lang
%dir %{_localstatedir}/lib/brltty
%config(noreplace) %{_sysconfdir}/brltty.conf
%{_sysconfdir}/brltty/
%exclude %{_sysconfdir}/brltty/Initramfs
%{_unitdir}/brltty.service
%{_bindir}/brltty
%{_bindir}/brltty-*
%exclude %{_bindir}/brltty-minimal
%{_libdir}/brltty/
%exclude %{_libdir}/brltty/libbrlttybba.so
%exclude %{_libdir}/brltty/libbrlttybxw.so
%exclude %{_libdir}/brltty/libbrlttyxa2.so
%exclude %{_libdir}/brltty/libbrlttysen.so
%exclude %{_libdir}/brltty/libbrlapi_java.so
%license LICENSE-LGPL
%doc %{_mandir}/man[15]/brltty.*
%{_sysconfdir}/X11/Xsession.d/90xbrlapi
%{_datadir}/polkit-1/actions/org.a11y.brlapi.policy
%{_datadir}/polkit-1/rules.d/org.a11y.brlapi.rules

%files minimal -f %{name}.lang
%config(noreplace) %{_sysconfdir}/brltty-minimal.conf
%{_sysconfdir}/brltty-minimal/
%{_bindir}/brltty-minimal
%{_libdir}/brltty-minimal/
%license LICENSE-LGPL

%files docs
%doc Documents/ChangeLog Documents/TODO
%doc Documents/Manual-BRLTTY/
%doc doc/*

%files xw
%doc Drivers/Braille/XWindow/README
%{_libdir}/brltty/libbrlttybxw.so

%files at-spi2
%{_libdir}/brltty/libbrlttyxa2.so

%files espeak-ng
%{_libdir}/brltty/libbrlttysen.so

%files -n brlapi
%{_bindir}/vstp
%{_bindir}/eutp
%{_bindir}/xbrlapi
%{_libdir}/brltty/libbrlttybba.so
%{_libdir}/libbrlapi.so.*
%ghost %verify(not group) %{_sysconfdir}/brlapi.key
%doc Drivers/Braille/XWindow/README
%doc Documents/Manual-BrlAPI/
%doc %{_mandir}/man1/xbrlapi.*
%doc %{_mandir}/man1/vstp.*
%doc %{_mandir}/man1/eutp.*

%files -n brlapi-devel
%{_libdir}/libbrlapi.so
%{_includedir}/brltty
%{_includedir}/brlapi*.h
%{_libdir}/pkgconfig/brltty.pc
%doc %{_mandir}/man3/brlapi_*.3*
%doc Documents/BrlAPIref/BrlAPIref/

%files -n tcl-brlapi
%{tcl_sitearch}/brlapi-%{api_version}

%files -n python3-brlapi
%{python3_sitearch}/brlapi.cpython-*.so
%{python3_sitearch}/Brlapi-%{api_version}-*.egg-info

%files -n brlapi-java
%{_libdir}/brltty/libbrlapi_java.so
%{_jnidir}/brlapi.jar

%files -n ocaml-brlapi
%{_libdir}/ocaml/brlapi/
%{_libdir}/ocaml/stublibs/

%files dracut
%{_prefix}/lib/dracut/modules.d/99brltty/
%dir %{_sysconfdir}/brltty/Initramfs
%config(noreplace) %verify(not size md5 mtime) %{_sysconfdir}/brltty/Initramfs/dracut.conf
%config(noreplace) %verify(not size md5 mtime) %{_sysconfdir}/brltty/Initramfs/cmdline


%changelog
* Mon Dec 23 2024 Rebuild Robot <rebot@opencloudos.org> - 6.6-9
- [Type] other
- [DESC] Rebuilt for icu

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 6.6-8
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 6.6-7
- Rebuilt for loongarch release

* Sat Jun 22 2024 Rebuild Robot <rebot@opencloudos.org> - 6.6-6
- [Type] other
- [DESC] Rebuilt for ocaml

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 6.6-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 6.6-4
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 23 2023 rockerzhu <rockerzhu@tencent.com> - 6.6-3
- Rebuilt for icu 73.2

* Wed Aug 23 2023 Miaojun Dong <zoedong@tencent.com> - %{pkg_version}-2
- Rebuild for at-spi2-core-2.49.90

* Fri Aug 04 2023 cunshunxia <cunshunxia@tencent.com> - 6.6-1
- upgrade to 6.6

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 6.4-2
- Rebuilt for OpenCloudOS Stream 23.05

* Thu Apr 06 2023 Miaojun Dong <zoedong@tencent.com> - 6.4-1
- initial build
